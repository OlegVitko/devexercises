Application has following third-party dependencies:
 - jQuery
 - Bootstrap
 - TestBox
 
Deploment steps:
1) place dev folder to the web root directory of CF
2) use following git command to get TestBox testing framework: git clone git://github.com/ortus-solutions/testbox testbox
3) put testbox directory to the web root of CF (near the dev directory)

Application Launching:
Go to <your_server>/dev/index.cfm (e.g. http://localhost:8500/dev/)

To exeute unit tests act in one of following ways: 
 - open <your_server>/dev/test/index.cfm (e.g. http://localhost:8500/dev/test/)
 - enter <your_server>/testbox/system/TestBox.cfc?method=runRemote&directory=dev.test.exercises (e.g. http://localhost:8500/testbox/system/TestBox.cfc?method=runRemote&directory=dev.test.exercises)
