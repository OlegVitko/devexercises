component displayName="Get Pivot Index" extends="testbox.system.BaseSpec" {

	function beforeTests() {
        search = new dev.exercises.GetPivotIndex();
	}

	function testArraySumEmptyArray() {
        var result = search.getArraySum([]);
        $assert.isEqual(result, 0);
	}

    function testArraySumRegular() {
        var result = search.getArraySum([1,2,3,4]);
        $assert.isEqual(result, 10);
    }

    function testCalculatePivotIndexFalse() {
        var result = search.calculatePivotIndex([3,1,2]);
        $assert.isEqual(result, -1);
    }

    function testCalculatePivotIndexTrue() {
        var result = search.calculatePivotIndex([3,1,3]);
        $assert.isEqual(result, 2);
    }

	function afterTests() {}
}