component displayName="Search For Address" extends="testbox.system.BaseSpec" {

	function beforeTests() {
		search = new dev.exercises.SearchForAddress();
		arrayForSearch = search.prepareData();
	}

	function testSearchForEmptyCriteria() {
		var result = search.searchForAddress(arrayForSearch, {firstName = "", birthDate = "",houseNum = "",postCode = ""});
		$assert.typeOf("array", result);
		$assert.isEqual(ArrayLen(result), ArrayLen(arrayForSearch));
	}

	function testSearchForWrongCriteria() {
		var result = search.searchForAddress(arrayForSearch, {firstName = "111",birthDate = "222",houseNum = "333",postCode = "444"});
		$assert.typeOf("array", result);
		$assert.isEmpty(result);
	}

	function testSearchForExactMatch() {
		var result = search.searchForAddress(arrayForSearch, {firstName = "Oleg",birthDate = "20/11/1987",houseNum = "7a",postCode = "77777"});
		$assert.typeOf("array", result);
		$assert.lengthOf(result, 1);
		$assert.typeOf("struct", result[1]);
		$assert.key(result[1], "firstName");
		$assert.key(result[1], "lastName");
		$assert.key(result[1], "birthDate");
		$assert.key(result[1], "country");
		$assert.key(result[1], "region");
		$assert.key(result[1], "city");
		$assert.key(result[1], "street");
		$assert.key(result[1], "houseNum");
		$assert.key(result[1], "postCode");
	}

	// CF10+
	/*function testSearchForWrongArgument() {
		$assert.throws(function() {
				search.searchForAddress(arrayForSearch, {qqq = "Oleg"});
			});
	}*/

	function afterTests() {}
}
