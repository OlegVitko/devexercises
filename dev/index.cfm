<html>
<head>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

	<script src="js/main.js"></script>
</head>
<body>
    <ul class="nav nav-tabs" id="exerciseTabs">
        <li class="active"><a href="#ex1Tab" data-toggle="tab">Exercise 1</a></li>
        <li><a href="#ex2Tab" data-toggle="tab">Exercise 2</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="ex1Tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <form id="exercise1Form" class="form-group">
                            <label for="listOfNumbers">List Of Numbers: </label>
                            <input name="listOfNumbers" placeholder="enter comma delimited numeric values" id="listOfNumbers" class="form-control"  >
                        </form>

                        <input type="button" name="search" value="Check For Pivot" onclick="javascript:submitExercise1()" class="btn btn-primary"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="ex2Tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <form id="exercise2Form" class="form-horizontal">
                            <div class="form-group">
                                <label for="firstName" class="col-sm-3 control-label">First Name: </label>
                                <div class="col-sm-5">
                                    <input name="firstName" placeholder="e.g. Oleg" id="firstName" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group disabled hidden">
                                <label for="lastName" class="col-sm-3 control-label">Last Name: </label>
                                <div class="col-sm-5">
                                    <input name="lastName" id="lastName" class="form-control" disabled >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="birthDate" class="col-sm-3 control-label">Date Of Birth: </label>
                                <div class="col-sm-5">
                                    <input name="birthDate" placeholder="20/11/1987" id="birthDate" class="form-control"  >
                                </div>
                            </div>
                            <div class="form-group disabled hidden">
                                <label for="country" class="col-sm-3 control-label">Country: </label>
                                <div class="col-sm-5">
                                    <input name="country" id="country" class="form-control" disabled >
                                </div>
                            </div>
                            <div class="form-group disabled hidden">
                                <label for="region" class="col-sm-3 control-label">Region: </label>
                                <div class="col-sm-5">
                                    <input name="region" id="region" class="form-control" disabled >
                                </div>
                            </div>
                            <div class="form-group disabled hidden">
                                <label for="city" class="col-sm-3 control-label">City: </label>
                                <div class="col-sm-5">
                                    <input name="city" id="city" class="form-control" disabled >
                                </div>
                            </div>
                            <div class="form-group disabled hidden">
                                <label for="street" class="col-sm-3 control-label">Street: </label>
                                <div class="col-sm-5">
                                    <input name="street" id="street" class="form-control" disabled >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="houseNum" class="col-sm-3 control-label">House Number: </label>
                                <div class="col-sm-5">
                                    <input name="houseNum" placeholder="e.g. 7a" id="houseNum" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="postCode" class="col-sm-3 control-label">Post Code: </label>
                                <div class="col-sm-5">
                                    <input name="postCode" placeholder="e.g 77777" id="postCode" class="form-control" >
                                </div>
                            </div>
                        </form>

                        <div class="row">
                            <div class="col-md-2">
                                <input type="button" name="search" value="Search"  onclick="javascript:submitExercise2()" class="btn btn-primary"/>
                            </div>
                                <div id="manyResultWarn" class="hidden col-md-6 alert alert-warning alert-dismissible">
                                    <h4>Warning</h4>
                                    Please consider that your query returns more then one record
                                </div>
                                <div id="noResultWarn" class="hidden col-md-6 alert alert-warning alert-dismissible">
                                    <h4>Warning</h4>
                                    Your query has not returned any records
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalWindow" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</body>
</html>
