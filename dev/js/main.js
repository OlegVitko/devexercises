function init() {
    $(function(){
        $('#birthDate').datepicker({
            changeYear: true,
            dateFormat: "dd/mm/yy",
            yearRange: "1970:2016"});
    });

    $('#exerciseTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
}

init();

function submitExercise1() {
    $.ajax({
        type : "POST",
        data : $("#exercise1Form").serialize(),
        url : "exercises/getPivotIndex.cfm",
        success : handlePivotResult,
        error : handleError
    });
}

function handlePivotResult(text) {
    $('.modal-title').text('Result');
    $('.modal-body').text(text);
    $('#modalWindow').modal();
}

function submitExercise2() {
    $.ajax({
        type : "POST",
        data : $("#exercise2Form").serialize(),
        url : "exercises/searchForAddress.cfm",
        success : handleSearchResult,
        error : handleError
    });
}

function handleSearchResult(text) {
    qResultArray = JSON.parse(text);

    // if no records found
    if (qResultArray.length == 0) {
        $('#noResultWarn').removeClass('hidden');
        $('#manyResultWarn').addClass('hidden');

        $('.form-group.disabled').addClass('hidden');
    } else if (qResultArray.length >= 1) {
        person = qResultArray[0];

        if (qResultArray.length > 1) {
            $('#manyResultWarn').removeClass('hidden');
        } else {
            $('#manyResultWarn').addClass('hidden');
        }
        $('#noResultWarn').addClass('hidden');

        $('.form-group.disabled').removeClass('hidden');


        $('#firstName').val(person.FIRSTNAME);
        $('#lastName').val(person.LASTNAME);
        $('#birthDate').val(person.BIRTHDATE);
        $('#country').val(person.COUNTRY);
        $('#region').val(person.REGION);
        $('#city').val(person.CITY);
        $('#street').val(person.STREET);
        $('#houseNum').val(person.HOUSENUM);
        $('#postCode').val(person.POSTCODE);
    }
}

function handleError(html, type, message) {
    $('.modal-title').text('Error');
    $('.modal-body').text(message);
    $('#modalWindow').modal();
}