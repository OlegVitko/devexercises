component {

    public function prepareData() {
        var result = arrayNew(1);

        result[1] = {
            firstName = "Oleg",
            lastName = "Vitko",
            birthDate = "20/11/1987",
            country = "Ukraine",
            region = "Kyiv",
            city = "Kyiv",
            street = "Kikvidze",
            houseNum = "7a",
            postCode = "77777"};
        result[2] = {
            firstName = "Alexander",
            lastName = "Ivanov",
            birthDate = "20/11/1988",
            country = "Ukraine",
            region = "Poltava",
            city = "Velyka Bagachka",
            street = "Gagarina",
            houseNum = "8a",
            postCode = "77778"};

        return result;
    }

    public function searchForAddress(Array arrayForSearch, Struct frm) {
        var result = arrayNew(1);

        for (var i = 1; i LTE ArrayLen(arrayForSearch); i++) {
            var element = arrayForSearch[i];

            if (((element.firstName EQ frm.firstName) OR (frm.firstName == ""))
            AND ((element.birthDate EQ frm.birthDate) OR (frm.birthDate == ""))
            AND ((element.houseNum EQ frm.houseNum) OR (frm.houseNum == ""))
            AND ((element.postCode EQ frm.postCode) OR (frm.postCode == ""))) {
                arrayAppend(result, element);
            }
        }

        return result;
    }
}