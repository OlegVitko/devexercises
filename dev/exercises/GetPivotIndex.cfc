component {

    public function getArraySum(Array arrayForSearch) {
        var result = 0;

        for (var i = 1; i LTE ArrayLen(arrayForSearch); i++) {
            if (isNumeric(arrayForSearch[i])) {
                result = result + arrayForSearch[i];
            } else {
                // this is why ArraySum was not used for
                throw(message = "element " & i & " could not be casted to number");
            }
        }

        return result;
    }

    public function calculatePivotIndex(Array arrayForSearch) {
        var result = -1;

        var sum1 = 0;
        var sum2 = getArraySum(arrayForSearch);

        for (var i = 1; i LTE ArrayLen(arrayForSearch); i++) {
            sum2 = sum2 - arrayForSearch[i];
            if (sum1 EQ sum2) {
                result = i;
                break;
            }

            sum1 = sum1 + arrayForSearch[i];
        }

        return result;
    }
}