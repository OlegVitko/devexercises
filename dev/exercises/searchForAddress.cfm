<cfsetting showdebugoutput="false">

<cfif IsDefined("FORM.firstName") && IsDefined("FORM.birthDate") && IsDefined("FORM.houseNum") && IsDefined("FORM.postCode")>

    <cfscript>
        search = new SearchForAddress();

        arrayForSearch = search.prepareData();

        queryResult = search.searchForAddress(arrayForSearch, FORM);
        result = serializeJSON(queryResult);
    </cfscript>

    <cfoutput>#result#</cfoutput>
<cfelse>
	<cfoutput>No necessary argument(s) specified</cfoutput>
</cfif>
