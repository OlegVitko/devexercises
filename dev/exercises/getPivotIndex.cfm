<cfsetting showdebugoutput="false">

<cfif IsDefined("FORM.listOfNumbers")>
    <cfset arrayForSearch = "#listToArray(FORM.listOfNumbers, ",")#">

    <cfscript>
        search = new GetPivotIndex();

        message = "There is no pivot element in array";

        pivotIdx = search.calculatePivotIndex(arrayForSearch);

        if (pivotIdx GT -1) {
            message = "At least one pivot element exists in array. It is on position " & pivotIdx &
            " and has value " & arrayForSearch[pivotIdx];
        }
    </cfscript>

    <cfoutput>#message#</cfoutput>
<cfelse>
    <cfoutput>No necessary argument(s) specified</cfoutput>
</cfif>


